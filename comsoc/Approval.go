package comsoc

// ApprovalSWF Vote par approbation
// thresholds est un slice d'entiers strictement positifs
func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {
	count = make(Count)
	err = checkProfileFromProfile(p)
	if err != nil {
		return nil, err
	}
	//initialisation de la map : comptes à 0
	count = make(Count)
	for _, alt := range p[0] {
		count[alt] = 0
	}
	//actualisation du compte à l'aide du scrutin
	for index, alt := range p {
		for i := 0; i < thresholds[index]; i++ {
			count[alt[i]] += 1
		}
	}
	return
}

func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	alts, err := ApprovalSWF(p, thresholds)
	if err != nil {
		return nil, err
	}
	bestAlts = maxCount(alts)
	return
}
