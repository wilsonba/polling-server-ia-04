package comsoc

import (
	"math/rand"
)

func GenerateProfile(nVoters int, nAlts int) (profile Profile) {
	// Initialisation du profil
	profile = make([][]Alternative, nVoters)
	for i := range profile {
		profile[i] = make([]Alternative, nAlts)
	}

	// Définition des alternatives
	values := make([]Alternative, nAlts)
	for i := range values {
		values[i] = Alternative(i + 1)
	}

	// Remplissage du profil avec des permutations différentes à chaque rang
	for i := range profile {
		// Mélange les valeurs pour chaque ligne
		permutation := make([]Alternative, len(values))
		copy(permutation, values)
		rand.Shuffle(len(permutation), func(i, j int) {
			permutation[i], permutation[j] = permutation[j], permutation[i]
		})

		// Remplit la ligne avec la permutation
		copy(profile[i], permutation)
	}
	return profile
}
