package comsoc
import ("fmt")

// Majorité simple
func MajoritySWF(p Profile) (count Count, err error) {
	err = checkProfileFromProfile(p)
	if err != nil {
		return nil, err
	}
	//initialisation de la map : comptes à 0
	count = make(Count)
	for _,alt := range p[0]{
		count[alt]=0
	}
	//actualisation du compte à l'aide du scrutin
	for _, pref := range p {
		count[pref[0]]++
	}
	return
}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p)
	if err != nil {
		return nil, err
	}
	return maxCount(count), err
}

func Test_majority() {
	profil := GenerateProfile(3, 5)
	fmt.Println("Profil :", profil)
	count, _ := MajoritySWF(profil)
	fmt.Println("Décompte :", count)
	//winners, _ := MajoritySCF(profil)
	//fmt.Println("Vainqueur(s) :", winners)
}