# Serveur de vote Rest en Go

Le serveur permet de créer des scrutins suivants plusieures méthodes de vote.

Les votants peuvent faire des requêtes pour voter dans les scrutins qui le concernent

Toutes les requêtes doivent suivre la norme suivante : https://gitlab.utc.fr/lagruesy/ia04/-/blob/main/docs/sujets/activit%C3%A9s/serveur-vote/api.md

2 exécutables (indépendants) sont fournis :

* `launch-bagt` permet de lancer un agent de type serveur de vote
* `launch-vagt` permet de lancer un agent de type votant

Il est possible d'avoir accès à ces commandes en les installant ainsi :

`go get gitlab.utc.fr/wilsonba/polling-server-ia-04`

puis

`go install gitlab.utc.fr/wilsonba/polling-server-ia-04/cmd/launch-bagt`

Méthodes de vote implémentées :
* Majorité
* Approval
* Borda
* Copeland
* STV

Agents implémentés :
* Serveur
* Votants (paramètres à modifier en fonction du scrutin créé auprès du serveur)