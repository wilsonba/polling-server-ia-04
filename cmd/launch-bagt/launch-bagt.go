package main

import (
	"fmt"

	ba "gitlab.utc.fr/wilsonba/polling-server-ia-04/agt/ballotagent"
)

func main() {
	server := ba.NewBallotServerAgent(":8080")
	server.Start()
	fmt.Scanln()
}
