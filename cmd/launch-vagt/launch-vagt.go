package main

import (
	"fmt"

	va "gitlab.utc.fr/wilsonba/polling-server-ia-04/agt/voteragent"
)

func main() {
	ag := va.NewRandomRestClientAgent("ag1", "http://localhost:8080", "scrutin1", 3, 1)
	ag.Start()

	_, err := fmt.Scanln()
	if err != nil {
		return
	}
}
