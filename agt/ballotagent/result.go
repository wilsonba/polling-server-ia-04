package ballotagent

import (
	"encoding/json"
	"fmt"
	"net/http"

	rad "gitlab.utc.fr/wilsonba/polling-server-ia-04"
)

func (rsa *BallotServerAgent) sendResults(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()
	rsa.reqCount++

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := decodeRequest[rad.Ballot](r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// recupération des infos
	scrutin := req.BallotID
	voteBallot := rad.Ballot{BallotID: scrutin}
	ballots := make([]rad.Ballot, len(rsa.ballots))

	// Check if ballot exists
	i := 0
	for k := range rsa.ballots {
		ballots[i] = k
		i++
	}
	if !contains[rad.Ballot](ballots, voteBallot) {
		w.WriteHeader(http.StatusNotFound)
		msg := fmt.Sprintf("The ballot '%s' does not exist", scrutin)
		w.Write([]byte(msg))
		return
	}
	ballot := rsa.ballots[voteBallot]

	// Check que la deadline n'est pas déjà passée
	if ballot.isOpen {
		w.WriteHeader(http.StatusTooEarly)
		msg := fmt.Sprintf("The ballot '%s' is not closed yet.", scrutin)
		w.Write([]byte(msg))
		return
	}

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(rsa.ballots[voteBallot].results)
	w.Write(serial)
}
