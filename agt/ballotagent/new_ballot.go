package ballotagent

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	rad "gitlab.utc.fr/wilsonba/polling-server-ia-04"
	cs "gitlab.utc.fr/wilsonba/polling-server-ia-04/comsoc"
)

type BallotInfo struct {
	profile  cs.Profile
	options  [][]int
	votersId []string
	nbAlts   int
	isOpen   bool
	results  rad.ResultResponse
}

func (rsa *BallotServerAgent) createBallot(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()
	rsa.reqCount++

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := decodeRequest[rad.BallotRequest](r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	// Check for valid deadline formatting
	deadline, errTime := time.Parse(time.RFC3339, req.Deadline)
	if errTime != nil {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("'%s' n'utilise pas le bon format, merci d'utiliser RFC3339 ", req.Deadline)
		w.Write([]byte(msg))
		return
	}
	// Check if the deadline is in the future
	if deadline.Before(time.Now()) {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("'%s' est déjà passé ", req.Deadline)
		w.Write([]byte(msg))
		return
	}

	// traitement de la requête
	var resp rad.Ballot

	resp.BallotID = fmt.Sprintf("scrutin%d", len(rsa.ballots)+1)
	rsa.ballots[resp] = BallotInfo{
		profile:  make(cs.Profile, 0),
		options:  make([][]int, 0),
		votersId: req.VotersID,
		nbAlts:   req.NbAlts,
		isOpen:   true,
		results:  rad.ResultResponse{}}

	tb := make([]cs.Alternative, 0)
	for _, alt := range req.TieBreak {
		tb = append(tb, cs.Alternative(alt))
	}

	switch req.Rule {
	case "majority":
		go rsa.handleBallot(resp, cs.MajoritySWF, cs.MajoritySCF, tb, deadline)
	case "borda":
		go rsa.handleBallot(resp, cs.BordaSWF, cs.BordaSCF, tb, deadline)
	case "approval":
		go rsa.handleBallotWithSingleOption(resp, cs.ApprovalSWF, cs.ApprovalSCF, tb, deadline)
	case "copeland":
		go rsa.handleBallot(resp, cs.CopelandSWF, cs.CopelandSCF, tb, deadline)
	case "stv":
		go rsa.handleBallot(resp, cs.STV_SWF, cs.STV_SCF, tb, deadline)
	default:
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Unkonwn rule '%s'", req.Rule)
		w.Write([]byte(msg))
		return
	}

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *BallotServerAgent) handleBallot(
	ballot rad.Ballot,
	swf func(cs.Profile) (cs.Count, error),
	scf func(cs.Profile) ([]cs.Alternative, error),
	orderedTBAlts []cs.Alternative,
	deadline time.Time,
) {
	time.Sleep(time.Until(deadline))
	targetBallot := rsa.ballots[ballot]
	profile := targetBallot.profile

	// If profile is empty, set winner as 0 and ranking as empty list
	if len(profile) == 0 {
		targetBallot.results = rad.ResultResponse{Winner: 0, Ranking: make([]int, 0)}
		fmt.Println(ballot.BallotID, "n'a pas reçu de votes.")
		return
	}

	tb := cs.TieBreakFactory(orderedTBAlts)
	ballotSWF := cs.SWFFactory(swf, tb)
	ballotSCF := cs.SCFFactory(scf, tb)
	ranking, _ := ballotSWF(profile)
	winner, err := ballotSCF(profile)
	if err != nil {
		fmt.Println(err)
		return
	}
	intRanking := make([]int, 0)
	for _, alt := range ranking {
		intRanking = append(intRanking, int(alt))
	}

	rsa.ballots[ballot] = BallotInfo{
		profile:  profile,
		options:  targetBallot.options,
		votersId: targetBallot.votersId,
		nbAlts:   targetBallot.nbAlts,
		isOpen:   false,
		results:  rad.ResultResponse{Winner: int(winner), Ranking: intRanking},
	}
}

func (rsa *BallotServerAgent) handleBallotWithSingleOption(
	ballot rad.Ballot,
	swf func(cs.Profile, []int) (cs.Count, error),
	scf func(cs.Profile, []int) ([]cs.Alternative, error),
	orderedTBAlts []cs.Alternative,
	deadline time.Time,
) {
	time.Sleep(time.Until(deadline))
	targetBallot := rsa.ballots[ballot]
	targetBallot.isOpen = false
	profile := targetBallot.profile
	options := targetBallot.options

	// If profile is empty, set winner as 0 and ranking as empty list
	if len(profile) == 0 {
		targetBallot.results = rad.ResultResponse{Winner: 0, Ranking: make([]int, 0)}
		fmt.Println(ballot.BallotID, "n'a pas reçu de votes.")
		return
	}

	// Only the fist element of the agents' options is filled/matters
	singleOptions := make([]int, len(options))
	for i, option := range options {
		singleOptions[i] = option[0]
	}

	tb := cs.TieBreakFactory(orderedTBAlts)
	ballotSWF := cs.SWFFactoryWithOptions(swf, tb)
	ballotSCF := cs.SCFFactoryWithOptions(scf, tb)
	ranking, _ := ballotSWF(profile, singleOptions)
	winner, err := ballotSCF(profile, singleOptions)
	if err != nil {
		fmt.Println(err)
		return
	}
	intRanking := make([]int, 0)
	for _, alt := range ranking {
		intRanking = append(intRanking, int(alt))
	}

	rsa.ballots[ballot] = BallotInfo{
		profile:  profile,
		options:  targetBallot.options,
		votersId: targetBallot.votersId,
		nbAlts:   targetBallot.nbAlts,
		isOpen:   false,
		results:  rad.ResultResponse{Winner: int(winner), Ranking: intRanking},
	}
}
