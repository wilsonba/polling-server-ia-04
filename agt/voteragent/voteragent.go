package voteragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"

	rad "gitlab.utc.fr/wilsonba/polling-server-ia-04"
	cs "gitlab.utc.fr/wilsonba/polling-server-ia-04/comsoc"
)

type RestClientAgent struct {
	id          string
	url         string
	ballotName  string
	nbAlts      int
	preferences []int
	options     []int
}

func NewRestClientAgent(id string, url string, ballotName string, nbAlts int, preferences []int, options []int) *RestClientAgent {
	return &RestClientAgent{id, url, ballotName, nbAlts, preferences, options}
}

func NewRandomRestClientAgent(id string, url string, ballotName string, nbAlts int, nbOptions int) *RestClientAgent {
	newProfile := cs.GenerateProfile(1, nbAlts)
	preferences := make([]int, 0)
	for _, alternative := range newProfile[0] {
		preferences = append(preferences, int(alternative))
	}

	options := make([]int, 0)
	for i := 0; i < nbOptions; i++ {
		options = append(options, rand.Intn(nbAlts))
	}

	return NewRestClientAgent(id, url, ballotName, nbAlts, preferences, options)
}

func (rca *RestClientAgent) vote() (err error) {
	req := rad.Vote{
		AgentID:  rca.id,
		BallotID: rca.ballotName,
		Prefs:    rca.preferences,
		Options:  rca.options,
	}

	// sérialisation de la requête
	url := rca.url + "/vote"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}

	return
}

func (rca *RestClientAgent) Start() {
	log.Printf("démarrage de %s", rca.id)

	err := rca.vote()

	if err != nil {
		log.Fatal(rca.id, "error:", err.Error())
	} else {
		log.Printf("[%s] voted in %s\n", rca.id, rca.ballotName)
	}
}
